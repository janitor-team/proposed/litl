Source: litl
Priority: optional
Maintainer: Samuel Thibault <sthibault@debian.org>
Build-Depends: debhelper-compat (= 12)
Build-Depends-Indep: texlive-latex-base, texlive-latex-recommended, texlive-latex-extra,
  texlive-fonts-recommended, texlive-pictures,
  latexmk, doxygen, libjs-jquery
Rules-Requires-Root: no
Standards-Version: 4.5.0
Section: libs
Homepage: https://github.com/trahay/LiTL
Vcs-Git: https://salsa.debian.org/debian/litl.git
Vcs-Browser: https://salsa.debian.org/debian/litl

Package: litl-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${shlibs:Depends}, ${misc:Depends}, libjs-jquery
Description: Lightweight Trace Library - documentation
 LiTL is a lightweight tool for recording events during the execution of
 scientific high-performance applications.
 .
 This package contains the documentation.

Package: litl-tools
Section: science
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: litl-doc
Description: Lightweight Trace Library - tools
 LiTL is a lightweight tool for recording events during the execution of
 scientific high-performance applications.
 .
 This package contains tools for manipulating trace files.

Package: liblitl-dev
Section: libdevel
Architecture: any
Depends: liblitl0 (= ${binary:Version}), ${misc:Depends}
Recommends: litl-doc
Conflicts: libfxt-dev
Description: Lightweight Trace Library - development files
 LiTL is a lightweight tool for recording events during the execution of
 scientific high-performance applications.
 .
 This package contains files for developments with litl.

Package: liblitl0
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Lightweight Trace Library - shared library
 LiTL is a lightweight tool for recording events during the execution of
 scientific high-performance applications.
 .
 This package contains the litl shared library.
